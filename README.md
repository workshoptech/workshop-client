# Workshop app

> This platform was developed to handle the most important processes about workshops, like subscriptions, registers, users, messages, reports for internship program and so on.

We are using Vue framework for development and vuetify integration to keep an standard pattern design.


## Getting Started

Please refer to the [contributing](./CONTRIBUTING.md) file to see our contribution guidelines and also our [workflow](./WORKFLOW.md) file to see our development workflow.

## Environment Setup

Workshop app requires Vue version 2.6.6 or higher.

*How to install Vue*
https://vuejs.org/v2/guide/installation.html

### Project setup
**Note:** Please contact the project leader in order to get all the **Firebase** *`keys.json`* to execute the services.

At your terminal, you will need to execute:

```
npm install
npm run serve
```

Now the project is going to be running in localhost.
