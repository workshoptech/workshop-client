/**
 * API definition
 */
const baseUrl = "https://workshop-api-rest.herokuapp.com"
const apiVersion = "/api"
const apiUrl = baseUrl + apiVersion

/**
 * API GITHUB
 */
const githubBaseUrl = "https://api.github.com"
const githubApiVersion = ""
const githubApiUrl = githubBaseUrl + githubApiVersion

/**
 * API path type
 */
const sessionPath = "/auth"
const reportPath = "/tasks"
const workshopPath = "/workshops"
const approvalRequestsPath = "/approval_requests"
const accountPath = "/users"
const profilePath = "/auth"
const messagesPath = "/messages"
const notificationsPath = "/notifications"

/**
 * GITHUB API path type
 */
const repo = "/repos/WorkshopTechnology/Materiales/contents/Talleres"

/**
 * Public constants object
 */
export default {
  Endpoints: {
    Session: {
      login: apiUrl + sessionPath + "/sign_in",
      signUp: apiUrl + sessionPath,
      signOut: apiUrl + sessionPath + "/sign_out"
    },
    Report: {
      getReports: apiUrl + reportPath,
      getDetailedReport: apiUrl + "/users/{user_id}" + reportPath
    },
    Workshop: {
      getSignedWorkshops: apiUrl + workshopPath + "/signed",
      getBriefWorkshops: apiUrl + workshopPath + "/brief",
      getWorkshop: apiUrl + workshopPath + "/{workshop_id}",
      createWorkshop: apiUrl + workshopPath,
      updateWorkshop: apiUrl + workshopPath + "/{workshop_id}",
      deleteWorkshop: apiUrl + workshopPath + "/{workshop_id}",
      subscribeWorkshop: apiUrl + workshopPath + "/{workshop_id}/subscribe",
      unsubscribeWorkshop: apiUrl + workshopPath + "/{workshop_id}/unsubscribe",
      mineWorkshops: apiUrl + workshopPath + "/mine"
    },
    ApprovalRequests: {
      getApprovalRequests: apiUrl + workshopPath + approvalRequestsPath,
      getDetailedApprovalRequest: apiUrl + workshopPath + "/{workshop_id}" + approvalRequestsPath + "/{approval_request_id}",
      createApprovalRequest: apiUrl + workshopPath + "/{workshop_id}" + approvalRequestsPath,
      updateApprovalRequest: apiUrl + workshopPath + "/{workshop_id}" + approvalRequestsPath + "/{approval_request_id}",
      deleteApprovalRequest: apiUrl + workshopPath + "/{workshop_id}" + approvalRequestsPath + "/{approval_request_id}",
    },
    Account: {
      getAccounts: apiUrl + accountPath,
      getDetailedAccount: apiUrl + accountPath + "/{account_id}",
      updateAccount: apiUrl + accountPath + "/{account_id}",
      deleteAccount: apiUrl + accountPath + "/{account_id}"
    },
    Profile: {
      getProfile: apiUrl + profilePath,
      updateProfile: apiUrl + profilePath
    },
    Documentation: {
      getWorkshopContent: githubBaseUrl + repo
    },
    Messages: {
      getDestinataries: apiUrl + messagesPath + "/destinataries",
      getMessages: apiUrl + messagesPath,
      getDetailedMessage: apiUrl + messagesPath + "/{message_id}",
      sendMessage: apiUrl + messagesPath
    },
    Notifications: {
      getNotifications: apiUrl + notificationsPath,
      getLastNotifications: apiUrl + notificationsPath + "/brief"
    }
  }
}
