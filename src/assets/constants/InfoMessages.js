export default {
  ErrorMessage: {
    dataNoAvailable: "Data is not available, try again",
    requestFailed: "Request has failed, try again",
    wrongCredentials: "Invalid login credentials. Please try again.",
    wrongSignUp: "That email has already been taken.",
  },
  Message: {
    accountUpdated: "Account updated successfully",
    accountDeleted: "Account deleted successfully",
    workshopCreated: "Workshop created successfully",
    workshopUpdated: "Workshop updated successfully",
    workshopDeleted: "Workshop updated successfully",
    workshopSubscribed: "You've been subscribed",
    workshopUnsubscribed: "You've been unsubscribed",
    workshopApproved: "Workshop approved",
    workshopRejected: "Workshop rejected",
    messageSent: "Your message has been sent successfully"
  }
}
