export default {
  isRequiredRule: function (fieldName) {
    return v => !!v || fieldName + " required."
  },
  isValidEmail: function () {
    return v => /.+@.+/.test(v) || ' Email has to be valid.'
  }
}
