import Vue from "vue"
import "./plugins/vuetify"
import App from "./App.vue"
import VueRouter from "vue-router"
import routes from './routes'
import VueSweetalert2 from 'vue-sweetalert2'
import "./stylus/main.styl"
import VueTour from 'vue-tour'

require('vue-tour/dist/vue-tour.css')

Vue.use(VueRouter)
Vue.use(VueSweetalert2)
Vue.use(VueTour)

const router = new VueRouter({
  mode: "history",
  routes: routes
})

Vue.config.productionTip = false;

export default new Vue({
  router,
  render: h => h(App)
}).$mount("#app")
