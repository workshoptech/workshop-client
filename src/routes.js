import Login from "./scenes/Session/scenes/Login";
import Register from "./scenes/Session/scenes/Register";
import Home from "./scenes/Dashboard/Home/Home";
import Leaderboard from "./scenes/Dashboard/Leaderboard/Leaderboard";
import WorkshopDetail from "./scenes/Dashboard/WorkshopDetail/WorkshopDetail";
import Admin from "./scenes//Dashboard/Admin/Admin";
import ApprovalRequest from "./scenes/Dashboard/Admin/scenes/ApprovalRequest/ApprovalRequest";
import Accounts from "./scenes/Dashboard/Admin/scenes/Accounts/Accounts";
import Workshops from "./scenes/Dashboard/Admin/scenes/Workshops/Workshops";
import AllWorkshops from "./scenes/Dashboard/Workshops/Workshops";
import Messages from "./scenes/Dashboard/Admin/scenes/Messages/Messages";
import Reports from "./scenes/Dashboard/Admin/scenes/Reports/Reports";
import UserReport from "./scenes/Dashboard/Admin/scenes/Reports/scenes/UserReport/UserReport";
import ProfileConfiguration from "./scenes/Profile/Settings/Settings";
import AllNotifications from "./scenes/Profile/Notifications/Notifications";
import MyWorkshops from "./scenes/Profile/MyWorkshops/MyWorkshops"
import MyWorkshop from "./scenes/Profile/MyWorkshops/scenes/MyWorkshop/MyWorkshop"

export default [
  {path: "/", name: "login", component: Login},
  {path: "/register", name: "register", component: Register},
  {path: "/home", name: "home", component: Home},
  {path: "/workshop/:id/:is_preview?", name: "workshop", component: WorkshopDetail},
  {path: "/workshops", name: "workshops", component: AllWorkshops},
  {path: "/leaderboard", name: "leaderboard", component: Leaderboard},
  {
    path: "/admin",
    name: "admin",
    component: Admin,
    children: [
      {path: "requests", name: "approval-request", component: ApprovalRequest},
      {path: "accounts", name: "accounts", component: Accounts},
      {path: "workshops", name: "workshopsAdmin", component: Workshops},
      {path: "messages", name: "messages", component: Messages},
      {path: "reports", name: "reports", component: Reports},
      {
        path: "reports/:userId",
        name: "user-report",
        component: UserReport
      }
    ]
  },
  {path: "/configuration", name: "configuration", component: ProfileConfiguration},
  {path: "/notifications", name: "notifications", component: AllNotifications},
  {path: "/my-workshops", name: "my-workshops", component: MyWorkshops},
  {path: "/my-workshop/:id", name: "my-workshop", component: MyWorkshop}
];
