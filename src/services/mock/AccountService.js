//TODO: Implement this mock service
export default class AccountService {

  constructor() {
  }

  getAccounts(callback) {
  }

  getDetailedAccount(accountId, callback) {
  }

  updateAccount(account, callback) {
  }

  deleteAccount(accountId, callback) {
  }
}
