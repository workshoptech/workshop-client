//TODO: Implement this mock service
export default class ApprovalRequestService {

  constructor() {

  }

  getApprovalRequests(callback) {
    return new Promise((resolve, reject) => {
      resolve([
        {
          "id": "01",
          "comment": "Pues meh!",
          "status": "Pending",
          "speaker": {
            "id": "01",
            "name": "Marco",
            "lastname": "Chávez",
            "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"
          },
          "workshop": {
            "id": "01",
            "name": "Command terminal basics",
            "banner_url": "",
            "description": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
            "begin_date": "1565506518",
            "end_date": null,
          }
        },
        {
          "id": "02",
          "comment": "Pues meh!",
          "status": "Pending",
          "speaker": {
            "id": "02",
            "name": "Marco",
            "lastname": "Chávez",
            "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"
          },
          "workshop": {
            "id": "01",
            "name": "Command terminal basics",
            "banner_url": "",
            "description": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
            "begin_date": "1565506518",
            "end_date": null,
          }
        }
      ])
    })
  }
}
