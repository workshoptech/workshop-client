//TODO: Implement this mock service
export default class WorkshopService {

  constructor() {

  }

  getSignedWorkshops() {
    return new Promise((resolve, reject) => {
      resolve([
        {
          "id": "01",
          "name": "Git basics part 1",
          "banner_url": "https://miro.medium.com/max/1000/1*8xkaNeNQHvWA6OEmjOMavw.jpeg",
          "begin_date": "1565502854 ",
          "end_date": null,
          "status": "signed",
          "speaker_id": 1,
          "speaker_name": "Marco Chávez",
          "subscribers": [
            {"id": "01", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "02", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "03", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "04", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "05", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "06", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
          ]
        }
      ])
    })
  }

  getBriefWorkshops() {
    return new Promise((resolve, reject) => {
      resolve([
        {
          "id": "01",
          "name": "Git basics part 1",
          "banner_url": "https://miro.medium.com/max/1000/1*8xkaNeNQHvWA6OEmjOMavw.jpeg",
          "begin_date": "1565502854 ",
          "end_date": null,
          "status": "coming",
          "speaker_id": 1,
          "speaker_name": "Marco Chávez",
          "subscribers": [
            {"id": "01", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "02", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "03", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "04", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "05", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "06", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
          ]
        },
        {
          "id": "02",
          "name": "Git basics part 1",
          "banner_url": "https://miro.medium.com/max/1000/1*8xkaNeNQHvWA6OEmjOMavw.jpeg",
          "begin_date": "1565502854 ",
          "end_date": null,
          "status": "past",
          "speaker_id": 1,
          "speaker_name": "Marco Chávez",
          "subscribers": [
            {"id": "01", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "02", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "03", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "04", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "05", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "06", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
          ]
        },
        {
          "id": "03",
          "name": "Git basics part 2",
          "banner_url": "https://miro.medium.com/max/1000/1*8xkaNeNQHvWA6OEmjOMavw.jpeg",
          "begin_date": "1565502854 ",
          "end_date": null,
          "status": "passed",
          "speaker_id": 1,
          "speaker_name": "Marco Chávez",
          "subscribers": [
            {"id": "01", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "02", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "03", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "04", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "05", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
            {"id": "06", "name": "Marco", "photo_url": "https://avatars0.githubusercontent.com/u/22420715?s=460&v=4"},
          ]
        }
      ])
    })
  }

  getLatestWorkshops(callback) {
  }

  updateWorkshop(workshop, callback) {
  }

  deleteWorkshop(workshopId, callback) {
  }
}
