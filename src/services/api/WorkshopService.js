import AuthManager from '../session/AuthManager'
import _ from '../utils/EndpointsHandler'
import view from '../utils/ViewHandler'
import axios from 'axios'

export default class WorkshopService {

  constructor() {
    this.authManager = new AuthManager()
  }

  getSignedWorkshops() {
    const self = this
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.get(_.getSignedWorkshopsEndpoint(), headers)
        .then((response) => {
          if (response.status === 200) {
            resolve(response.data)
          } else {
            reject(response.statusText)
          }
        })
        .catch((error) => {
          reject(error.message)
        })
    })
  }

  getBriefWorkshops() {
    const self = this
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.get(_.getBriefWorkshopsEndpoint(), headers)
        .then((response) => {
          if (response.status === 200) {
            resolve(response.data)
          } else {
            reject(response.statusText)
          }
        })
        .catch((error) => {
          reject(error.message)
        })
    })
  }

  getWorkshop(workshopId) {
    const self = this
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.get(_.getWorkshopEndpoint(workshopId), headers)
        .then((response) => {
          if (response.status === 200) {
            resolve(response.data)
          } else {
            reject(response.statusText)
          }
        })
        .catch((error) => {
          reject(error.message)
        })
    })
  }

  createWorkshop(workshop) {
    const self = this
    view.showProgressBar(true)
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.post(_.getCreateWorkshopEndpoint(), workshop, headers)
        .then((response) => {
          if (response.status === 201) {
            view.showProgressBar(false)
            resolve(response.data)
          } else {
            view.showProgressBar(false)
            reject(response.statusText)
          }
        })
        .catch((error) => {
          view.showProgressBar(false)
          reject(error.message)
        })
    })
  }

  updateWorkshop(workshop) {
    const self = this
    view.showProgressBar(true)
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.put(_.getUpdateWorkshopEndpoint(workshop.id), workshop, headers)
        .then((response) => {
          if (response.status === 200) {
            view.showProgressBar(false)
            resolve(response.data)
          } else {
            view.showProgressBar(false)
            reject(response.statusText)
          }
        })
        .catch((error) => {
          view.showProgressBar(false)
          reject(error.message)
        })
    })
  }

  deleteWorkshop(workshopId) {
    const self = this
    view.showProgressBar(true)
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.delete(_.getDeleteWorkshopEndpoint(workshopId), headers)
        .then((response) => {
          if (response.status === 204) {
            view.showProgressBar(false)
            resolve(response.data)
          } else {
            view.showProgressBar(false)
            reject(response.statusText)
          }
        })
        .catch((error) => {
          view.showProgressBar(false)
          reject(error.message)
        })
    })
  }

  getFolderWorkshopDocumentation(workshopName) {
    return new Promise((resolve, reject) => {
      axios.get(_.getGithubApiEndpoint() + "/" + workshopName)
        .then((response) => {
          if (response.status === 200) {
            resolve(response.data);
          }
        })
    })
  }

  getFolderWorkshopDocumentationImages(workshopName) {
    return new Promise((resolve, reject) => {
      axios.get(_.getGithubApiEndpoint() + "/" + workshopName + "/Images")
        .then((response) => {
          if (response.status === 200) {
            resolve(response.data);
          }
        })
    })
  }

  subscribeWorkshop(workshopId) {
    const self = this
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.post(_.getSubscribeWorkshopEndpoint(workshopId), {}, headers)
        .then((response) => {
          if (response.status === 200) {
            resolve(response.data)
          } else {
            reject(response.statusText)
          }
        })
        .catch((error) => {
          reject(error.message)
        })
    })
  }

  unsubscribeWorkshop(workshopId) {
    const self = this
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.post(_.getUnsubscribeWorkshopEndpoint(workshopId), {}, headers)
        .then((response) => {
          if (response.status === 200) {
            resolve(response.data)
          } else {
            reject(response.statusText)
          }
        })
        .catch((error) => {
          reject(error.message)
        })
    })
  }

  getMineWorkshops() {
    const self = this
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.get(_.getMineWorkshopsEndpoint(), headers)
        .then((response) => {
          if (response.status === 200) {
            resolve(response.data)
          } else {
            reject(response.statusText)
          }
        })
        .catch((error) => {
          reject(error.message)
        })
    })
  }
}
