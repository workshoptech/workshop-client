import AuthManager from '../session/AuthManager'
import _ from '../utils/EndpointsHandler'
import axios from 'axios';
import view from '../utils/ViewHandler'

export default class MessagesService {

  constructor() {
    this.authManager = new AuthManager()
  }

  getMessagesDestinataries() {
    const self = this
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.get(_.getMessagesDestinatariesEndpoint(), headers)
        .then((response) => {
          if (response.status === 200) {
            resolve(response.data)
          } else {
            reject(response.statusText)
          }
        })
        .catch((error) => {
          reject(error.message)
        })
    })
  }

  getMessages() {
    const self = this
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.get(_.getMessagesEndpoint(), headers)
        .then((response) => {
          if (response.status === 200) {
            resolve(response.data)
          } else {
            reject(response.statusText)
          }
        })
        .catch((error) => {
          reject(error.message)
        })
    })
  }

  getDetailedMessage(messageId) {
    const self = this
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.get(_.getDetailedMessageEndpoint(messageId), headers)
        .then((response) => {
          if (response.status === 200) {
            resolve(response.data)
          } else {
            reject(response.statusText)
          }
        })
        .catch((error) => {
          reject(error.message)
        })
    })
  }

  sendMessage(message) {
    const self = this
    view.showProgressBar(true)
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.post(_.getSendMessageEndpoint(), message, headers)
        .then((response) => {
          if (response.status === 200) {
            view.showProgressBar(false)
            resolve(response.data)
          } else {
            view.showProgressBar(false)
            reject(response.statusText)
          }
        })
        .catch((error) => {
          view.showProgressBar(false)
          reject(error.message)
        })
    })
  }
}
