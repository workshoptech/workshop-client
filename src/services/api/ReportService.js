import AuthManager from '../session/AuthManager'
import _ from '../utils/EndpointsHandler'
import axios from 'axios'

export default class ReportService {
  constructor() {
    this.authManager = new AuthManager()
  }

  getReports() {
    const self = this
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.get(_.getReportsEndpoint(), headers)
        .then((response) => {
          if (response.status === 200) {
            resolve(response.data)
          } else {
            reject(response.statusText)
          }
        })
        .catch((error) => {
          reject(error.message)
        })
    })
  }

  getDetailedReport(userId) {
    const self = this
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.get(_.getDetailedReportEndpoint(userId), headers)
        .then((response) => {
          if (response.status === 200) {
            resolve(response.data)
          } else {
            reject(response.statusText)
          }
        })
        .catch((error) => {
          reject(error.message)
        })
    })
  }
}
