import AuthManager from "../session/AuthManager";
import _ from "../utils/EndpointsHandler";
import axios from "axios";

export default class AccountService {
  constructor() {
    this.authManager = new AuthManager();
  }

  getNotifications() {
    const self = this
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.get(_.getNotificationsEndpoint(), headers)
        .then(response => {
          if (response.status === 200) {
            resolve(response.data);
          } else {
            reject(response.statusText)
          }
        })
        .catch(error => {
          reject(error.message)
        });
    });
  }

  getLastNotifications() {
    const self = this;
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders();
      axios.get(_.getLastNotificationsEndpoint(), headers)
        .then(response => {
          if (response.status === 200) {
            resolve(response.data)
          } else {
            reject(response.statusText)
          }
        })
        .catch(error => {
          reject(error.message)
        })
    })
  }
}
