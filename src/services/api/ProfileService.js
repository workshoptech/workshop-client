import AuthManager from '../session/AuthManager'
import _ from '../utils/EndpointsHandler'
import axios from 'axios';
import view from '../utils/ViewHandler'

export default class ProfileService {

  constructor() {
    this.authManager = new AuthManager()
  }

  getProfile() {
    const self = this
    view.showProgressBar(true)
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.get(_.getProfileEndpoint(), headers)
        .then((response) => {
          if (response.status === 200) {
            view.showProgressBar(false)
            resolve(response.data)
          } else {
            view.showProgressBar(false)
            reject(response.statusText)
          }
        })
        .catch((error) => {
          view.showProgressBar(false)
          reject(error.message)
        })
    })
  }

  updateProfile(profile) {
    const self = this
    view.showProgressBar(true)
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.put(_.getUpdateProfileEndpoint(), profile, headers)
        .then((response) => {
          if (response.status === 200) {
            view.showProgressBar(false)
            resolve(response.data)
          } else {
            view.showProgressBar(false)
            reject(response.statusText)
          }
        })
        .catch((error) => {
          view.showProgressBar(false)
          reject(error.message)
        })
    })
  }
}
