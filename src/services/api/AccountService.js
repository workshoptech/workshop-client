import AuthManager from '../session/AuthManager'
import _ from '../utils/EndpointsHandler'
import axios from 'axios';
import view from '../utils/ViewHandler'

export default class AccountService {

  constructor() {
    this.authManager = new AuthManager()
  }

  getAccounts() {
    const self = this
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.get(_.getAccountsEndpoint(), headers)
        .then((response) => {
          if (response.status === 200) {
            resolve(response.data)
          } else {
            reject(response.statusText)
          }
        })
        .catch((error) => {
          reject(error.message)
        })
    })
  }

  getDetailedAccount(accountId) {
    const self = this
    view.showProgressBar(true)
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.get(_.getDetailedAccountEndpoint(accountId), headers)
        .then((response) => {
          if (response.status === 200) {
            view.showProgressBar(false)
            resolve(response.data)
          } else {
            view.showProgressBar(false)
            reject(response.statusText)
          }
        })
        .catch((error) => {
          view.showProgressBar(false)
          reject(error.message)
        })
    })
  }

  updateAccount(account) {
    const self = this
    view.showProgressBar(true)
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.put(_.getUpdateAccountEndpoint(account.id), account, headers)
        .then((response) => {
          if (response.status === 200) {
            view.showProgressBar(false)
            resolve(response.data)
          } else {
            view.showProgressBar(false)
            reject(response.statusText)
          }
        })
        .catch((error) => {
          view.showProgressBar(false)
          reject(error.message)
        })
    })
  }

  deleteAccount(accountId) {
    const self = this
    view.showProgressBar(true)
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.delete(_.getDeleteAccountEndpoint(accountId), headers)
        .then((response) => {
          if (response.status === 204) {
            view.showProgressBar(false)
            resolve(response.data)
          } else {
            view.showProgressBar(false)
            reject(response.statusText)
          }
        })
        .catch((error) => {
          view.showProgressBar(false)
          reject(error.message)
        })
    })
  }
}
