import AuthManager from '../session/AuthManager'
import _ from '../utils/EndpointsHandler'
import axios from 'axios';
import view from '../utils/ViewHandler'

export default class RequestService {

  constructor() {
    this.authManager = new AuthManager()
  }

  getApprovalRequests() {
    const self = this
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.get(_.getApprovalRequestsEndpoint(), headers)
        .then((response) => {
          if (response.status === 200) {
            resolve(response.data)
          } else {
            reject(response.statusText)
          }
        })
        .catch((error) => {
          reject(error.message)
        })
    })
  }

  getDetailedApprovalRequest(workshopId, approvalRequestId) {
    const self = this
    view.showProgressBar(true)
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.get(_.getDetailedApprovalRequestEndpoint(workshopId, approvalRequestId), headers)
        .then((response) => {
          if (response.status === 200) {
            view.showProgressBar(false)
            resolve(response.data)
          } else {
            view.showProgressBar(false)
            reject(response.statusText)
          }
        })
        .catch((error) => {
          view.showProgressBar(false)
          reject(error.message)
        })
    })
  }

  createApprovalRequest(approvalRequest) {
    const self = this
    view.showProgressBar(true)
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.post(_.getCreateApprovalRequestEndpoint(approvalRequest.workshopId), approvalRequest, headers)
        .then((response) => {
          if (response.status === 200) {
            view.showProgressBar(false)
            resolve(response.data)
          } else {
            view.showProgressBar(false)
            reject(response.statusText)
          }
        })
        .catch((error) => {
          view.showProgressBar(false)
          reject(error.message)
        })
    })
  }

  updateApprovalRequest(approvalRequest) {
    const self = this
    view.showProgressBar(true)
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      const workshopId = approvalRequest.approval_request.workshop.id
      const approvalId = approvalRequest.approval_request.id
      axios.put(_.getUpdateApprovalRequestEndpoint(workshopId, approvalId), approvalRequest, headers)
        .then((response) => {
          if (response.status === 200) {
            view.showProgressBar(false)
            resolve(response.data)
          } else {
            view.showProgressBar(false)
            reject(response.statusText)
          }
        })
        .catch((error) => {
          view.showProgressBar(false)
          reject(error.message)
        })
    })
  }

  deleteApprovalRequest(approvalRequest) {
    const self = this
    view.showProgressBar(true)
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      axios.put(_.getUpdateApprovalRequestEndpoint(approvalRequest.workshop.id, approvalRequest.id),
        message, headers)
        .then((response) => {
          if (response.status === 200) {
            view.showProgressBar(false)
            resolve(response.data)
          } else {
            view.showProgressBar(false)
            reject(response.statusText)
          }
        })
        .catch((error) => {
          view.showProgressBar(false)
          reject(error.message)
        })
    })
  }
}
