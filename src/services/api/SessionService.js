import AuthManager from '../session/AuthManager'
import ProfileDataManager from '../session/ProfileDataManager'
import _ from '../utils/EndpointsHandler'
import axios from 'axios';
import view from '../utils/ViewHandler'

export default class SessionService {

  constructor() {
    this.authManager = new AuthManager()
    this.profileDataManager = new ProfileDataManager()
    this.headers = {
      "Content-Type": "application/json"
    }
    this.saveSession = (response) => {
      this.authManager.saveHeaders(response.headers)
      this.profileDataManager.saveProfileData(response.data.data)
    }
    this.hasExpired = () => {
      let expiry = this.authManager.getHeaders().expiry
      let expiryDate = new Date(expiry * 1000)
      let today = new Date()
      return today > expiryDate
    }
  }

  login(user) {
    const self = this
    view.showProgressBar(true)
    return new Promise((resolve, reject) => {
      axios.post(_.getLoginEndpoint(), user, self.headers)
        .then((response) => {
          if (response.status === 200) {
            self.saveSession(response)
            view.showProgressBar(false)
            resolve(response.data)
          } else {
            reject(response.status)
          }
        })
        .catch((error) => {
          view.showProgressBar(false)
          reject(error.response.status)
        })
    })
  }

  signUp(user) {
    const self = this
    view.showProgressBar(true)
    return new Promise((resolve, reject) => {
      axios.post(_.getSignUpEndpoint(), user, self.headers)
        .then((response) => {
          if (response.status === 200) {
            view.showProgressBar(false)
            self.saveSession(response)
            resolve(response.data)
          } else {
            view.showProgressBar(false)
            reject(response.status)
          }
        })
        .catch((error) => {
          view.showProgressBar(false)
          reject(error.response.status)
        })
    })
  }

  signOut() {
    const self = this
    view.showProgressBar(true)
    return new Promise((resolve, reject) => {
      let headers = self.authManager.getHeaders()
      if (self.hasExpired()) {
        view.showProgressBar(false)
        localStorage.clear()
        resolve("")
      } else {
        axios.delete(_.getSignOutEndpoint(), headers)
          .then((response) => {
            if (response.status === 200) {
              view.showProgressBar(false)
              localStorage.clear()
              resolve(response.data)
            } else {
              view.showProgressBar(false)
              reject(response.statusText)
            }
          })
          .catch((error) => {
            view.showProgressBar(false)
            reject(error.message)
          })
      }
    })
  }
}
