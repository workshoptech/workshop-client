export default class ProfileDataManager {
  constructor() {
  }

  saveProfileData(profile) {
    localStorage.setItem('id', profile['id'])
    localStorage.setItem('name', profile['name'])
    localStorage.setItem('lastname', profile['lastname'])
    localStorage.setItem('email', profile['email'])
    localStorage.setItem('phone', profile['phone'])
    localStorage.setItem('photo_url', profile['photo_url'])
    localStorage.setItem('trello_token', profile['trello_token'])
    localStorage.setItem('trello_id', profile['trello_id'])
    localStorage.setItem('trello_username', profile['trello_username'])
    localStorage.setItem('role', profile['role'])
  }

  updateSimpleProfileData(profile) {
    localStorage.setItem('name', profile['name'])
    localStorage.setItem('lastname', profile['lastname'])
    localStorage.setItem('email', profile['email'])
    localStorage.setItem('photo_url', profile['photo_url'])
    localStorage.setItem('trello_username', profile['trello_username'])
  }

  getProfileData() {
    let profile = {
      'id': localStorage.getItem('id'),
      'name': localStorage.getItem('name'),
      'lastname': localStorage.getItem('lastname'),
      'email': localStorage.getItem('email'),
      'phone': localStorage.getItem('phone'),
      'photo_url': localStorage.getItem('photo_url'),
      'trello_token': localStorage.getItem('trello_token'),
      'trello_id': localStorage.getItem('trello_id'),
      'trello_username': localStorage.getItem('trello_username'),
      'role': localStorage.getItem('role')
    }
    return profile
  }

  updateOnboardingStatus(status) {
    localStorage.setItem('onboarding_status', status)
  }

  updateAdminOnboardingStatus(status) {
    localStorage.setItem('onboarding_admin_status', status)
  }

  getOnboardingStatus() {
    return localStorage.getItem('onboarding_status')
  }

  getAdminOnboardingStatus() {
    return localStorage.getItem('onboarding_admin_status')
  }
}
