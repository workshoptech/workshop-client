export default class AuthManager {
  constructor() {
  }

  saveHeaders(header) {
    localStorage.setItem('access-token', header['access-token'])
    localStorage.setItem('token-type', header['token-type'])
    localStorage.setItem('client', header['client'])
    localStorage.setItem('expiry', header['expiry'])
    localStorage.setItem('uid', header['uid'])
  }

  getHeaders() {
    let headers = {
      headers: {
        'access-token': localStorage.getItem('access-token'),
        'token-type': localStorage.getItem('token-type'),
        'client': localStorage.getItem('client'),
        'expiry': localStorage.getItem('expiry'),
        'uid': localStorage.getItem('uid')
      }
    }
    return headers
  }
}
