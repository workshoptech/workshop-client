import Swal from 'sweetalert2'

export default {
  showProgressBar: (_isVisible) => {
    if (_isVisible) {
      Swal.fire({
        title: 'Loading',
        customClass: {
          title: "title"
        },
        allowEscapeKey: false,
        allowOutsideClick: false,
        onOpen: () => {
          Swal.showLoading();
        }
      })
    } else {
      Swal.close()
    }
  },

  showMessage: (_typeMessage, _title) => {
    Swal.fire({
      title: _title,
      type: _typeMessage,
      customClass: {
        title: "title"
      },
      showConfirmButton: false,
      allowEscapeKey: false,
      allowOutsideClick: false,
      timer: 2000
    })
  }
}
