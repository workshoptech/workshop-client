import _ from '../../assets/constants/ApiRest'

export default {
 getLoginEndpoint: () => {
  return _.Endpoints.Session.login
 },
 getSignUpEndpoint: () => {
  return _.Endpoints.Session.signUp
 },
 getSignOutEndpoint: () => {
  return _.Endpoints.Session.signOut
 },

 getAccountsEndpoint: () => {
  return _.Endpoints.Account.getAccounts
 },
 getDetailedAccountEndpoint: (accountId) => {
  return _.Endpoints.Account.getDetailedAccount.replace("{account_id}", accountId)
 },
 getUpdateAccountEndpoint: (accountId) => {
  return _.Endpoints.Account.updateAccount.replace("{account_id}", accountId)
 },
 getDeleteAccountEndpoint: (accountId) => {
  return _.Endpoints.Account.deleteAccount.replace("{account_id}", accountId)
 },

 getProfileEndpoint: () => {
  return _.Endpoints.Profile.getProfile
 },
 getUpdateProfileEndpoint: () => {
  return _.Endpoints.Profile.updateProfile
 },

 getReportsEndpoint: () => {
  return _.Endpoints.Report.getReports
 },
 getDetailedReportEndpoint: (userId) => {
  return _.Endpoints.Report.getDetailedReport.replace("{user_id}", userId)
 },

 getSignedWorkshopsEndpoint: () => {
  return _.Endpoints.Workshop.getSignedWorkshops
 },
 getBriefWorkshopsEndpoint: () => {
  return _.Endpoints.Workshop.getBriefWorkshops
 },
 getWorkshopEndpoint: (workshopId) => {
  return _.Endpoints.Workshop.getWorkshop.replace("{workshop_id}", workshopId)
 },
 getCreateWorkshopEndpoint: () => {
  return _.Endpoints.Workshop.createWorkshop
 },
 getUpdateWorkshopEndpoint: (workshopId) => {
  return _.Endpoints.Workshop.updateWorkshop.replace("{workshop_id}", workshopId)
 },
 getDeleteWorkshopEndpoint: (workshopId) => {
  return _.Endpoints.Workshop.deleteWorkshop.replace("{workshop_id}", workshopId)
 },
 getSubscribeWorkshopEndpoint: (workshopId) => {
  return _.Endpoints.Workshop.subscribeWorkshop.replace("{workshop_id}", workshopId)
 },
 getUnsubscribeWorkshopEndpoint: (workshopId) => {
  return _.Endpoints.Workshop.unsubscribeWorkshop.replace("{workshop_id}", workshopId)
 },
 getMineWorkshopsEndpoint: () => {
  return _.Endpoints.Workshop.mineWorkshops
 },

 getApprovalRequestsEndpoint: () => {
  return _.Endpoints.ApprovalRequests.getApprovalRequests
 },
 getDetailedApprovalRequestEndpoint: (workshopId, approvalRequestId) => {
  return _.Endpoints.ApprovalRequests.getDetailedApprovalRequest
    .replace("{workshop_id}", workshopId)
    .replace("{approval_request_id}", approvalRequestId)
 },
 getCreateApprovalRequestEndpoint: (workshopId) => {
  return _.Endpoints.ApprovalRequests.createApprovalRequest.replace("{workshop_id}", workshopId)
 },
 getUpdateApprovalRequestEndpoint: (workshopId, approvalRequestId) => {
  return _.Endpoints.ApprovalRequests.updateApprovalRequest
    .replace("{workshop_id}", workshopId)
    .replace("{approval_request_id}", approvalRequestId)
 },

 getGithubApiEndpoint: () => {
  return _.Endpoints.Documentation.getWorkshopContent
 },

 getMessagesDestinatariesEndpoint: () => {
  return _.Endpoints.Messages.getDestinataries
 },
 getMessagesEndpoint: () => {
  return _.Endpoints.Messages.getMessages
 },
 getDetailedMessageEndpoint: (messageId) => {
  return _.Endpoints.Messages.getDetailedMessage.replace("{message_id}", messageId)
 },
 getSendMessageEndpoint: () => {
  return _.Endpoints.Messages.sendMessage
 },

 getNotificationsEndpoint: () => {
  return _.Endpoints.Notifications.getNotifications
 },
 getLastNotificationsEndpoint: () => {
  return _.Endpoints.Notifications.getLastNotifications
 }
}
