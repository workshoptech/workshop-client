import * as firebase from "firebase/app"
import "firebase/storage";
import ProfileDataManager from "../../../services/session/ProfileDataManager"
import UuidGenerator from "../../../services/utils/UuidGenerator"
import view from "../../../services/utils/ViewHandler"

export default class FirebaseCloudStorage {
  constructor() {
    if (!firebase.apps.length) {
      firebase.initializeApp(require("../keys.json"))
    }
  }

  uploadImage(file, reference, callback) {
    //Setup data and handlers.
    view.showProgressBar(true)
    const profileDataManager = new ProfileDataManager()
    const storageRef = firebase.storage().ref()

    //Setup reference path.
    const referenceId = profileDataManager.getProfileData().email
    if (reference === "user") {
      reference = "user/" + referenceId + "_profile.jpg"
    } else {
      reference = "workshop/" + referenceId + "_" + UuidGenerator.generateUUID() + ".jpg"
    }

    //Upload file and handle its states.
    const uploadTask = storageRef.child(reference).put(file)
    uploadTask.on('state_changed', function (snapshot) {
      switch (snapshot.state) {
        case firebase.storage.TaskState.PAUSED:
          console.log('Upload is paused')
          break;
        case firebase.storage.TaskState.RUNNING:
          console.log('Upload is running')
          break;
      }
    }, function (error) {
      //Handle error upload
      callback(null)
    }, function () {
      // Handle successful uploads on complete
      uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
        callback(downloadURL)
      })
    })
  }
}

